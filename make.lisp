(in-package :gdl-user)

;; FLAG left for reference purposes - would need to be reviewed & adjusted:

#+nil
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *replay-home* (make-pathname :name nil :type nil
                                           :defaults (glisp:source-pathname)))
  (unless (find-package :enterprise)
    (error "This only runs in an enterprise GDL~%"))
  (load-quicklisp)
  (pushnew (probe-file (merge-pathnames "../" *replay-home*))
           ql:*local-project-directories* :test #'equalp))


#+nil
(define-object replay-app (gdl-app)

  :input-slots ((deploy-hosts (list "my-server")))
  
  :computed-slots
  ((application-name "replay")

   (generate-application-args (list :include-compiler t
                                    :discard-compiler nil
                                    :runtime :partners))
   
   (include-swank? t)
   (build-level :gwl)
   (overwrite? t)
   (overwrite-validation-function t)
   (pre-load-form '(progn (require :smtp) (require :ssl)))
   (pre-make-function #'(lambda()(asdf:operate 'asdf:monolithic-compile-bundle-op :replay)))
   (application-fasls (asdf:output-files 'asdf:monolithic-compile-bundle-op :replay))


   (post-make-function 
    #'(lambda()

        (let ((dest (merge-pathnames "gdlinit.cl" (the destination-directory))))
          (when (probe-file dest) (delete-file dest))
   	  (uiop:copy-file (merge-pathnames "gdlinit.cl" (glisp:system-home :replay))
                          dest))

	(dolist (host (ensure-list (the deploy-hosts)))
	  
	  (uiop:run-program (format nil "ssh ~a mkdir -p ~~/zoo/" host))

	  (uiop:run-program (format nil "rsync -zav ~a ~a:~~/zoo/~a/" 
				    (the destination-directory)  host
				    (lastcar (pathname-directory (the destination-directory))))))))))

#+nil
(defun replay-app () (the-object (make-object 'replay-app) make!))
