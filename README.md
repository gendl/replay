# Gendl Replay

This is a replay utility which is meant to seed and run regression
tests for any web-based Gendl application.

## Recording with log-based recorder 

Our current approach is to use aserve's built-in debugging switches
via which we can easily add our required request and response data to
the standard log stream, whence we can try to parse it and create
regression test seed data from it.

To generate the "decorated" log file, it is necessary to turn on some
debug switches in the running aserve instance (this can be done from
e.g. a Slime Repl): 

The latest Gendl contains the function `enable-debug-emit` in the GWL
package, which can simply be called as such with no arguments:
`(gwl::enable-debug-emit)`

In the tradition of debug stuff, the symbol is not exported from the
package so we use double colons. The `gwl::enable-debug-emit` function
is defined as follows:

```lisp
(dolist 
  (switch 
    (list :xmit-server-request-command :xmit-server-request-headers :xmit-server-request-body
	      :xmit-server-response-headers :xmit-server-response-body))
 (net.aserve::debug-on switch))
```

(which you can just copy/paste if you are using an older Gendl which
doesn't contain `enable-debug-emit`. 

## Parsing The Log File As Prepared With Above Debug Switches

Use the `parse-log-file` function defined in `./log-based/source/parse-log.lisp`

This function is step one in the process of massaging the data into
usable regression seed data, which is done by currently
under-development functions such as `group-requests`.

## Playing Back Recorded Requests And Regression Testing Against Recorded Responses

This will be filled in shortly. 

## Recording with legacy recorder.lisp

The toplevel file `recorder.lisp` contains a discrete and direct
lisp-based way to record requests for later replaying. However, this
utility does not include a way to record the responses for regression
test seeding. It's left here for legacy purposes.

