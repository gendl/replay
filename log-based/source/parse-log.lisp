(in-package :gdl-user)

(defun parse-log-file (log-file-path)
  (let (request-headerss request-commands request-bodies response-headerss response-bodies)
    (with-open-file (stream log-file-path)
      (do ((line (read-line stream nil) (read-line stream nil)))
	  ((null line) (list :request-headerss request-headerss
			     :request-commands request-commands
			     :request-bodies request-bodies
			     :response-headerss response-headerss
			     :response-bodies response-bodies))
        (cond
	  
          ;; Detect the start of a new request
          ((glisp:match-regexp "\\[xmit-server-request-command\\] (.*)" line)
	   (push line request-commands))

	  ((glisp:match-regexp "\\[xmit-server-request-headers\\] (.*)" line)
	   (push line request-headerss))
	  
	  ((glisp:match-regexp "\\[xmit-server-request-body\\] (.*)" line)
	   (push line request-bodies))

	  ((glisp:match-regexp "\\[xmit-server-response-headers\\] (.*)" line)
	   (push line response-headerss))

	  ((glisp:match-regexp "\\[xmit-server-response-body\\] (.*)" line)
	   (push line response-bodies)))))))
	  

(defun group-requests (plist)
   "Group data in the input plist into a list of plists, one per request."
  (let ((requests (make-hash-table :test 'equal)))
    ;; Helper to process entries
    (labels ((process-entry (key lines)
               (dolist (line lines)
                 (let ((worker-id (extract-worker-id line)))
                   (when worker-id
                     (let ((request (or (gethash worker-id requests)
                                        (setf (gethash worker-id requests)
                                              (list)))))
                       (setf (gethash worker-id requests)
                             (plist-put request key line))))))))
      ;; Process each key in the plist
      (dolist (key-value (loop for (key . values) on plist by #'cddr
                               collect (cons key values)))
        (process-entry (car key-value) (cdr key-value)))
      ;; Convert hash table to list of plists
      (loop for request in (hash-table-values requests)
            collect request))))

(defun extract-worker-id (line)
  "Extract the worker ID (e.g., `23-aserve-worker`) from a log line."
  (let ((regex "\\[(?:xmit-server-.*?)\\] ([0-9]+-aserve-worker):"))
    #+nil
    (ppcre:register-groups-bind  (worker-id) (regex line)
      worker-id)))

(defun plist-put (plist key value)
  "Add a key-value pair to a plist, appending the value if the key already exists."
  (let ((existing-values (getf plist key nil)))
    (if existing-values
        (setf (getf plist key) (append existing-values (list value)))
        (setf (getf plist key) (list value))))
  plist)


(defun parse-response-headers (line)
  "Parse a response headers log line to extract worker number, timestamp, pre/post type, and headers."
  (let ((regex "\\[xmit-server-response-headers\\] ([0-9]+)-aserve-worker: (.*?) - (pre\\|post) \\\"(.*)\\\"$"))
    (multiple-value-bind (matched groups)
        (ppcre:scan-to-strings regex line)
      (when matched
        (destructuring-bind (worker-number timestamp prepost headers) groups
          (list :worker-number (parse-integer worker-number)
                :timestamp timestamp
                :type (if (string= prepost "pre") :pre :post)
                :headers headers))))))






