;;
;; Copyright 2024 Genworks International
;;
;; This source file is part of the Gendl Replay web application
;; testing system
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 


(in-package :gwl)

(setq *aserve-log-request-after-functions* (list 'replay-log))

(defmethod net.aserve::log-request :after ((req http-request))
  (dolist (function gwl::*aserve-log-request-after-functions*)
    (funcall function req)))

(defparameter *replay-logging?* nil)

(let (replay-logging?)
  (defun replay-logging? () replay-logging?)
  (defun replay-logging-off ()(setq replay-logging? nil))
  (defun replay-logging-on () (setq replay-logging? t)))


(defmethod replay-log ((req http-request))
  
  (when (and (replay-logging?) front::*replay-log-stream*)
    (let* (;; (ipaddr (socket:remote-host (request-socket req)))
	   (end-time   (net.aserve::request-reply-date req))
	   (start-time (net.aserve::request-request-date req))
	   (code   (let ((object (net.aserve::request-reply-code req)))
		     (if object (net.aserve::response-number object) 999)))
           (headers (net.aserve::request-headers req))
           (reply-headers (net.aserve::request-reply-headers req))
	   (query-plist (gwl::assoc-list-to-plist (request-query req)))
           (body (net.aserve::request-request-body req))
           (uri (request-uri req))
	   (length  (or (net.aserve::request-reply-content-length req)
			(glisp:socket-bytes-written (request-socket req))))
	   ;;(referrer (net.aserve:header-slot-value req :referer))
	   ;;(user-agent (net.aserve::header-slot-value req :user-agent))
           )

      (let* ((log-plist 
	      (unless (let ((uri-path (net.aserve::uri-path uri)))
			(or (string-equal uri-path "/")
			    (string-equal uri-path "/send-remote-message")))
		(list ;;:ip-address (socket:ipaddr-to-dotted ipaddr)
		      :start-time start-time
		      :end-time end-time
		      :code code
                      :headers headers
                      :reply-headers reply-headers
                      :query-plist query-plist
                      :body body
		      :length length
		      :query (request-query req)
                      :uri (uri-to-string uri)
		      :uri-path (net.aserve::uri-path uri)
		      :uri-host (net.aserve::uri-host uri)
		      :uri-port (net.aserve::uri-port uri)
		      :uri-scheme (net.aserve::uri-scheme uri)
		      :method (net.aserve::request-method req)
		      ;;:referrer referrer
		      ;;:user-agent user-agent
                     ))))
	(when log-plist
          (mp:with-process-lock (front::*request-log-lock*)
            (pprint log-plist front::*replay-log-stream*))
          (force-output front::*replay-log-stream*))))))



